package com.progressoft.induction.atm;

import java.math.BigDecimal;

public class Client {

    private String accountNumber;
    private BigDecimal balance;

    Client(String accountNumber , BigDecimal balance){

        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
