package com.progressoft.induction.atm;


public class Currency {
   private Banknote banknote;
   private int paperNumber;

    Currency( Banknote banknote ,int paperNumber){
        this.banknote = banknote;
        this.paperNumber = paperNumber;
    }

    public Banknote getBanknote() {
        return banknote;
    }

    public void setBanknote(Banknote banknote) {
        this.banknote = banknote;
    }

    public int getPaperNumber() {
        return paperNumber;
    }

    public void setPaperNumber(int paperNumber) {
        this.paperNumber = paperNumber;
    }
}
