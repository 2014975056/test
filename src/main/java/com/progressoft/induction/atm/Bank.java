package com.progressoft.induction.atm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Bank{
    private BigDecimal  moneyInsideATM;
    private List<Currency> currencyList;
    private List<Banknote> withDrawnMoney;

    Bank(){
        currencyList = new ArrayList<>();
        currencyList.add(new Currency(Banknote.FIFTY_JOD ,10));
        currencyList.add(new Currency(Banknote.TWENTY_JOD ,20));
        currencyList.add(new Currency(Banknote.TEN_JOD ,100));
        currencyList.add(new Currency(Banknote.FIVE_JOD ,100));
        withDrawnMoney = new ArrayList<>();
        updateATM();
    }

    public void updateATM(){
        moneyInsideATM = computeMoneyInsideATM();
    }

    //Calculate the total money inside the ATM
    private BigDecimal computeMoneyInsideATM(){
        double tmp=0;
        for(int i=0;i<currencyList.size();i++){
            BigDecimal note= currencyList.get(i).getBanknote().getValue();
            int paperNumber = currencyList.get(i).getPaperNumber();
            tmp= tmp + note.doubleValue()*paperNumber;
        }
        moneyInsideATM = BigDecimal.valueOf(tmp);
        return moneyInsideATM;
    }

    public BigDecimal getMoneyInsideATM() {
        return moneyInsideATM;
    }

    public void setMoneyInsideATM(BigDecimal moneyInsideATM) {
        this.moneyInsideATM = moneyInsideATM;
    }

    public List<Banknote> getWithDrawnMoney() {
        return withDrawnMoney;
    }

    public List<Currency> getCurrencyList() {
        return currencyList;
    }

    public void setCurrencyList(List<Currency> currencyList) {
        this.currencyList = currencyList;
    }

    public void updateBank(Currency currency, int index){

        for (int i = 0; i < currency.getPaperNumber(); i++) {
                withDrawnMoney.add(currency.getBanknote());
               currencyList.get(index).setPaperNumber(currencyList.get(index).getPaperNumber()-1);
        }
    }

    //clear the withDrawnMoney list before you send next request
    public void clearWithDrawnMoney(){
        withDrawnMoney.clear();
    }

}


