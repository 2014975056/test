package com.progressoft.induction.atm.exceptions;

public class InsufficientFundsException extends RuntimeException{

    @Override
    public String toString(){
        return "Insufficient Funds Exception";
    }
}
