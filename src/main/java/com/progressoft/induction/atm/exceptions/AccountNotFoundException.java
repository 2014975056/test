package com.progressoft.induction.atm.exceptions;

public class AccountNotFoundException extends RuntimeException {

    @Override
    public String toString(){
     return "the account not found";
    }
}
