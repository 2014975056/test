package com.progressoft.induction.atm.exceptions;

public class NotEnoughMoneyInATMException extends RuntimeException {

    @Override
    public String toString(){
        return "Not Enough Money In ATM Exception";
    }
}
