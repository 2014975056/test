package com.progressoft.induction.atm;

import com.progressoft.induction.atm.exceptions.AccountNotFoundException;
import com.progressoft.induction.atm.exceptions.InsufficientFundsException;
import com.progressoft.induction.atm.exceptions.NotEnoughMoneyInATMException;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class ATMMachine implements BankingSystem, ATM {

    private Bank bank;
    private DataBase dataBase;
    private List<Client> clientList;


    ATMMachine() {
        //initialize the ATMMachine
        bank = new Bank();
        dataBase = new DataBase();
        clientList = dataBase.getClientList();
    }

    @Override
    public List<Banknote> withdraw(String accountNumber, BigDecimal amount) {

        BigDecimal accountBalance = getAccountBalance(accountNumber);
        if (amount.doubleValue() > accountBalance.doubleValue()) {
            throw new InsufficientFundsException();
        } else if (bank.getMoneyInsideATM().doubleValue() < amount.doubleValue()) {
            throw new NotEnoughMoneyInATMException();
        } else {
            bank.clearWithDrawnMoney();
            totalNumberOfNotes(amount);
            debitAccount(accountNumber, amount);
            bank.updateATM();
            return   bank.getWithDrawnMoney();

        }
    }

    @Override
    public BigDecimal getAccountBalance(String accountNumber) {

        Client clientListFiltered = filtering(accountNumber);
        BigDecimal balance = clientListFiltered.getBalance();
        return balance;

    }

    @Override
    public void debitAccount(String accountNumber, BigDecimal amount) {

        Client client = filtering(accountNumber);
        int index = clientList.indexOf(client);
        BigDecimal currentBalance = clientList.get(index).getBalance();
        BigDecimal newBalance = BigDecimal.valueOf(currentBalance.doubleValue() - amount.doubleValue());
        clientList.set(index, new Client(accountNumber, newBalance));
    }


    public Client filtering(String accountNumber) {

        List<Client> clientListFiltered = clientList.stream()
                .filter(client -> client.getAccountNumber().equals(accountNumber))
                .collect(Collectors.toList());
        if (clientListFiltered.size() == 1) {
            Client clientFiltered = clientListFiltered.get(0);
            return clientFiltered;
        } else {
            throw new AccountNotFoundException();
        }
    }

    private void totalNumberOfNotes(BigDecimal amount) {

        // function to count and
        // make with Drawn Money currency notes
        List<Currency> money = bank.getCurrencyList();
        int[] noteCounter = new int[money.size()];
        // check for notes.
        for (int i = 0; i < money.size(); i++) {

            // get instance from money of i
            BigDecimal bankNoteValue = money.get(i).getBanknote().getValue();
            int bankNotePapers = money.get(i).getPaperNumber();

            if(amount.compareTo( bankNoteValue ) >= 0 && amount.doubleValue()!=0){
                noteCounter[i] = (int) (amount.divide(bankNoteValue)).doubleValue() ;
                // count number of notes.
                if (noteCounter[i] > bankNotePapers) {
                    noteCounter[i] = bankNotePapers;
                }

                    if(noteCounter[i]>1 && (amount.doubleValue() - bankNoteValue.doubleValue() * noteCounter[i])==0){
                        if(i!=money.size()-1)
                        noteCounter[i]--;
                    }
                amount = BigDecimal.valueOf(amount.doubleValue() - bankNoteValue.doubleValue() * noteCounter[i]);  //finding the remaining amount

            }
            bank.updateBank(new Currency(money.get(i).getBanknote(),noteCounter[i]), i);
        }

    }

}






