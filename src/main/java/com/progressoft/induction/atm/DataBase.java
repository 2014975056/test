package com.progressoft.induction.atm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DataBase {

    private List <Client> clientList;

    DataBase(){
        //initialize Database of clients
        clientList = new ArrayList<>();
        clientList.add(new Client("123456789",new BigDecimal("1000.0")));
        clientList.add(new Client("111111111",new BigDecimal("1000.0")));
        clientList.add(new Client("222222222",new BigDecimal("1000.0")));
        clientList.add(new Client("333333333",new BigDecimal("1000.0")));
        clientList.add(new Client("444444444",new BigDecimal("1000.0")));
    }

    public List<Client> getClientList() {
        return clientList;
    }

    public void setClientList(List<Client> clientList) {
        this.clientList = clientList;
    }

}
